import React, { createContext, Component } from "react";

export const TransactionContext = createContext();

export class TransactionProvider extends Component {
  intialValue = () => {
    return {
      handleFilter: (filter,filterKey) => {
        this.props.handleFilter(filter, filterKey);
      }
    };
  };
  render() {
    return (
      <TransactionContext.Provider value={this.intialValue()}>
        {this.props.children}
      </TransactionContext.Provider>
    );
  }
}

