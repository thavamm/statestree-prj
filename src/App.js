import React from "react";
import Layout from "./components/Layout";
import Transaction from "./containers/Transaction";
import TransactionDetails from "./components/TransactionDetails";
import { BrowserRouter, Route, Switch } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div data-test="component-app">
        <Layout>
          <Switch>
            <Route path="/" exact component={Transaction} />
            <Route path="/account/:id" component={TransactionDetails} />
          </Switch>
        </Layout>
      </div>
    </BrowserRouter>
  );
}


export default App;
