import React from "react";
import classes from "./Loading.module.css";

const Loading = props => {
  return (
    <div className={classes.Loading}>
      <div className={classes.LoadingSpin} />
    </div>
  );
};

export default Loading;
