import { put, delay } from "redux-saga/effects";
import * as actions from "../actions";

/**
 * fetchTransactionSaga
 * 
 * @return {array} a list of transactions
 */
export function* fetchTransactionSaga() {
    const transactions = yield fetchTransactions()
    yield put(actions.fetchTransactions(transactions))
}

/**
 * fetchTransactions
 *
 * @return {array} a list of transactions
 */
export function* fetchTransactions() {
    try {
        yield delay(500)
        const result = yield fetch(
            "/data.json"
        ).then(res=>res.json())
        return result.transactions;
    } catch (error) {
        throw new Error("Unable fetch record from server!!");
    }
}
