import {takeLatest} from 'redux-saga/effects'

import * as actionTypes from '../actions/actionTypes'
import {fetchTransactionSaga} from './transactions'

export function* watchTransactions(){
    yield takeLatest(actionTypes.INITIATE_FETCH_TRANSACTION, fetchTransactionSaga)
}