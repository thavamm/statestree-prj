import { combineReducers } from 'redux';
import transactionReducer from './reducers/transactions';

/**
 * We can add multiple reducer here, I could have avoided this
 * as we have only one reducer, just adding this step for 
 * best practice.
 */
const rootReducer = combineReducers({
  transactions: transactionReducer,
});

export default rootReducer;