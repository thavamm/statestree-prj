import * as actionTypes from './actionTypes'

/**
 * @function :  fetchTransactions
 * @param {object} data:contain transactions data
 * @param {object} action:return the action object
 */
export const fetchTransactions = (data) =>{
    return {
        type: actionTypes.FETCH_TRANSACTION,
        data: data
    }
}
