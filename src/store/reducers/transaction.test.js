import * as actionTypes from "../actions/actionTypes";
import transactionReducer from "./transactions";

test("return default initial state of empty [] when no action is passed", () => {
  const newState = transactionReducer(null, {});
  expect(newState).toBe(null);
});

test("return state of transaction array upon receiving an action of type `FETCH_RANSACTION`", () => {
  const newState = transactionReducer(
    { transactions: [] },
    {
      type: actionTypes.FETCH_TRANSACTION,
      data: [{account:"2222"}] 
    }
  );
  expect(newState.transactions.length).not.toBe(0);
});
