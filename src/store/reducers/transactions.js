import * as actionTypes from "../actions/actionTypes";
const initialState = {
  transactions: [],
  loading: true,
  filterAccountNames:[],
  filterTransactionTypes:[]
};

/**
 * It will receive all the transactions data
 * @function :  reducer
 * @param {object} state:contain initial and final state of data
 * @param {object} action:return the action object
 */
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_TRANSACTION:
      return {
        ...state,
        transactions: action.data,
        loading: false
      };
    case actionTypes.UPDATE_FILTER_ACCOUNT_NAMES:
      return {
        ...state,
        filterAccountNames: action.data
      };
    case actionTypes.UPDATE_FILTER_TRANSATION_TYPES:
      return {
        ...state,
        filterTransactionTypes: action.data
      };
    default:
      return state;
  }
};

export default reducer;
