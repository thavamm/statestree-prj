import React from "react";
import { Link } from "react-router-dom";
import classes from "./HeaderBar.module.css";
const HeaderBar = props => {
  return (
    <header>
      <h2>{props.title}</h2>
      {props.back && <span className={classes.FloatRight}><Link className={classes.Back} to="/">Back</Link></span>}
      <hr />
    </header>
  );
};

export default HeaderBar;
