import React from "react";
import { connect } from "react-redux";
import HeaderBar from "./HeaderBar";
import classes from "./TransactionDetails.module.css";

const TransactionDetails = props => {
  console.log("props", props);

  const labelMap = {
    account: "Account No",
    accountName: "Account Name",
    currencyCode: "Currency Code",
    amount: "Amount",
    transactionType: "Transaction Type"
  };

  const accountNumber = props.match.params.id;
  const accountInfo = props.transactions.filter(
    transaction => transaction.account === accountNumber
  );

  const list = accountInfo.map(item => {
    return Object.keys(labelMap).map(key => {
      return (
        <li className={classes.Items__item} key={key}>
          <label>{labelMap[key]}</label>: <span>{item[key]}</span>
        </li>
      );
    });
  });

  const title = `Transaction ${accountNumber}`;
  return (
    <div data-test="component-transaction-details">
      <HeaderBar title={title} back={true} />
      <ul className={classes.Items}>{list}</ul>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    transactions: state.transactions.transactions
  };
};

export default connect(mapStateToProps)(TransactionDetails);
