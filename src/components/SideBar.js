import React from "react";
import PropTypes from "prop-types";
import FilterBar from "../containers/FilterBar";
import classes from "./SideBar.module.css";
import Loading from "../UI/Loading";

const SideBar = React.memo(props => {
  if (props.loading === true) {
    return (
      <div className={classes.Sidebar} data-test="component-sidebar">
        <h3>Filters</h3>
        <Loading />
      </div>
    );
  } else {
    return (
      <div className={classes.Sidebar} data-test="component-sidebar">
        <h3>Filters</h3>
        <FilterBar
          heading="Account Name"
          itemKey="accountName"
          items={props.accountNames}
        />
        <FilterBar
          heading="Transaction Type"
          itemKey="transactionType"
          items={props.transactionTypes}
        />
      </div>
    );
  }
});

SideBar.propTypes = {
  accountNames: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  ),
  transactionTypes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  )
};

export default SideBar;
