import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { findByTestAttr } from "../../test/testUtils";
import TransactionGrid from "./TransactionGrid";
configure({ adapter: new Adapter() });

const setup = (props = {}) => {
  const wrapper = shallow(<TransactionGrid {...props} />);
  return wrapper;
};

const transactions = [
  {
    account: "00022",
    accountName: "Test",
    transactionType: "Deposit",
    currencyCode: "ABC",
    amount: 333
  },
  {
    account: "00021",
    accountName: "Test2",
    transactionType: "Deposit2",
    currencyCode: "ABC2",
    amount: 444
  }
];
test("render without error", () => {
  const wrapper = setup({ transactions });
  const component = findByTestAttr(wrapper, "component-transaction-grid");
  expect(component.length).toBe(1);
});

test("render transaction grid table with <tr>", () => {
  const wrapper = setup({ transactions });
  const transactionNodes = findByTestAttr(wrapper, "transaction-row");
  expect(transactionNodes.length).toBe(transactions.length)
});
