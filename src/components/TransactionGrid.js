import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import classes from "./TransactionGrid.module.css";
import Loading from "../UI/Loading";

const TransactionGrid = React.memo(props => {
  const heading = (
    <thead className={classes.Heading}>
      <tr>
        <th>Account No.</th>
        <th>Account Name</th>
        <th>Currency</th>
        <th>Amount</th>
        <th>Transaction Type</th>
      </tr>
    </thead>
  );

  let list;
  if (props.loading === true) {
    list = (
      <tr>
        <td className={classes.LoadingPadding} colSpan="5">
          <Loading />
        </td>
      </tr>
    );
  } else {
    list = props.transactions.map(transaction => {
      return (
        <tr
          key={transaction.account + Math.random()}
          data-test="transaction-row"
        >
          <td>
            <Link to={`/account/${transaction.account}`}>
              {transaction.account}
            </Link>
          </td>
          <td>{transaction.accountName}</td>
          <td>{transaction.currencyCode}</td>
          <td>{transaction.amount}</td>
          <td>{transaction.transactionType}</td>
        </tr>
      );
    });
  }

  return (
    <div className={classes.Grid} data-test="component-transaction-grid">
      <table>
        {heading}
        <tbody>{list}</tbody>
      </table>
    </div>
  );
});

TransactionGrid.propTypes = {
  transactions: PropTypes.arrayOf(
    PropTypes.shape({
      account: PropTypes.string.isRequired,
      accountName: PropTypes.string.isRequired,
      amount: PropTypes.number.isRequired,
      transactionType: PropTypes.string.isRequired,
      currencyCode: PropTypes.string.isRequired
    })
  )
};

export default TransactionGrid;
