import React from "react";
import { shallow, configure, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { findByTestAttr } from "../../test/testUtils";
import SideBar from "./SideBar";
import FilterBar from "../containers/FilterBar";
configure({ adapter: new Adapter() });

const setup = (props = {}) => {
  const wrapper = shallow(<SideBar {...props} />);
  return wrapper;
};

test("render without error", () => {
  const wrapper = setup({});
  const component = findByTestAttr(wrapper, "component-sidebar");
  expect(component.length).toBe(1);
});

test("render 2 <FilterBar> components", () => {
    const wrapper = setup({});
    expect(wrapper.find(FilterBar).length).toBe(2)
});
