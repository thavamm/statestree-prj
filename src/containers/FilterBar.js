import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Checkbox from "../UI/Checkbox";
import { TransactionContext } from "../context/TransactionProvider";
import classes from "./FilterBar.module.css";

class FilterBar extends Component {
  state = {
    checkedItems: new Map()
  };

  handleChange = (event, handleFilter) => {
    const isChecked = event.target.checked;
    const name = event.target.name;
    this.setState(prevState => {
      const checkedItems = prevState.checkedItems.set(name, isChecked);
      handleFilter(checkedItems, this.props.itemKey);
      return {
        checkedItems
      };
    });
  };

  componentDidMount(){

    /**
     *  Maintaining previous selected checkbox 
     *  while come from Back button
     */
    if (
      this.props.filterAccountNames.length > 0 &&
      this.props.itemKey === "accountName"
    ) {
      this.props.filterAccountNames.forEach(el => {
        this.setState(prevState => {
          const checkedItems = prevState.checkedItems.set(el, true);
          return {
            checkedItems
          };
        });
      });
    }
    if (
      this.props.filterTransactionTypes.length > 0 &&
      this.props.itemKey === "transactionType"
    ) {
      this.props.filterTransactionTypes.forEach(el => {
        this.setState(prevState => {
          const checkedItems = prevState.checkedItems.set(el, true);
          return {
            checkedItems
          };
        });
      });
    }
  }


  render() {
    // console.log('classess=>',classes)
    return (
      <TransactionContext.Consumer>
        {context => (
          <div className={classes.FilterBar}>
            <h4>{this.props.heading}</h4>
            <ul className={classes.Items}>
              {this.props.items.map(item => (
                <li key={item.name}>
                  <Checkbox
                    name={item.name}
                    checked={this.state.checkedItems.get(item.name)}
                    onChange={e => this.handleChange(e, context.handleFilter)}
                  />
                  {item.name}
                </li>
              ))}
            </ul>
          </div>
        )}
      </TransactionContext.Consumer>
    );
  }
}

FilterBar.propTypes = {
  heading: PropTypes.string.isRequired,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  ),
  itemKey: PropTypes.string.isRequired
};

const mapStateToProps = state => {
  return {
    filterAccountNames: state.transactions.filterAccountNames,
    filterTransactionTypes: state.transactions.filterTransactionTypes
  };
};

export default connect(
  mapStateToProps,
  null
)(FilterBar);
