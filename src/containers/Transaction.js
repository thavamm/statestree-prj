import React, { Component, Suspense } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../store/actions/actionTypes";
import HeaderBar from "../components/HeaderBar";
import SideBar from "../components/SideBar";
import { TransactionProvider } from "../context/TransactionProvider";
import Loading from "../UI/Loading";
import classes from "./Transaction.module.css";

class Transaction extends Component {
  state = {
    loading: true,
    transactions: [],
    filterAccountNames: [],
    filterTransactionTypes: []
  };

  componentDidMount() {
    if (this.props.transactions.length === 0) {
      this.setState({ loading: true });
      this.props.onLoadTransactions();
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log(nextProps, nextState);
    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevProps, prevState, this.props.transactions);
  }

  extractData(key) {
    const values = [];
    this.props.transactions.forEach(transaction => {
      if (values.indexOf(transaction[key]) === -1) {
        values.push(transaction[key]);
      }
    });
    return values.map(item => ({ name: item }));
  }

  handleFilter = (filter, filterKey) => {
    //get only checked items
    const checkedItems = [];
    filter.forEach((val, key) => {
      if (val === true) {
        checkedItems.push(key);
      }
    });

    if (filterKey === "accountName") {
      this.setState({
        filterAccountNames: checkedItems
      });
      this.props.onUpdateFilterAccountNames(checkedItems);
    }

    if (filterKey === "transactionType") {
      this.setState({
        filterTransactionTypes: checkedItems
      });
      this.props.onUpdateFilterTransactionTypes(checkedItems);
    }

    let transactions = this.getTransactions(
      this.props.transactions,
      checkedItems,
      filterKey
    );
    if (
      filterKey === "accountName" &&
      this.props.filterTransactionTypes.length > 0
    ) {
      transactions = this.getTransactions(
        transactions,
        this.props.filterTransactionTypes,
        "transactionType"
      );
    } else if (
      filterKey === "transactionType" &&
      this.props.filterAccountNames.length > 0
    ) {
      transactions = this.getTransactions(
        transactions,
        this.props.filterAccountNames,
        "accountName"
      );
    }

    this.setState(prevState => ({
      transactions
    }));
  };

  getTransactions(transactions, checkedItems, filterKey) {
    return transactions.filter(transaction => {
      if (checkedItems.indexOf(transaction[filterKey]) > -1) {
        return true;
      } else {
        return false;
      }
    });
  }

  filterByStore(transactions) {
    if (transactions.length === 0) {
      return transactions;
    }
    if (this.props.filterAccountNames.length > 0) {
      transactions = this.getTransactions(
        transactions,
        this.props.filterAccountNames,
        "accountName"
      );
    }
    if (this.props.filterTransactionTypes.length > 0) {
      transactions = this.getTransactions(
        transactions,
        this.props.filterTransactionTypes,
        "transactionType"
      );
    }
    return transactions;
  }

  render() {
    let transactions =
      this.state.transactions.length > 0
        ? this.state.transactions
        : this.props.transactions;
    transactions = this.filterByStore(transactions);

    return (
      <React.Fragment>
        <TransactionProvider handleFilter={this.handleFilter}>
          <HeaderBar title="My Transaction" />
          <div className={classes.Container}>
            <SideBar
              accountNames={this.extractData("accountName")}
              transactionTypes={this.extractData("transactionType")}
              loading={this.props.loading}
            />
            {loadTransactionGrid(transactions, this.props.loading)}
          </div>
        </TransactionProvider>
      </React.Fragment>
    );
  }
}

const loadTransactionGrid = (transactions, loading) => {
  const TransactionGrid = React.lazy(() =>
    import("../components/TransactionGrid")
  );
  return (
    <Suspense
      fallback={
        <div className={classes.LoadingPadding}>
          <Loading />
        </div>
      }
    >
      <TransactionGrid loading={loading} transactions={transactions} />
    </Suspense>
  );
};

const mapStateToProps = state => {
  return {
    transactions: state.transactions.transactions,
    loading: state.transactions.loading,
    filterAccountNames: state.transactions.filterAccountNames,
    filterTransactionTypes: state.transactions.filterTransactionTypes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLoadTransactions: () => {
      return dispatch({ type: actionTypes.INITIATE_FETCH_TRANSACTION });
    },
    onUpdateFilterAccountNames: data => {
      return dispatch({ type: actionTypes.UPDATE_FILTER_ACCOUNT_NAMES, data });
    },
    onUpdateFilterTransactionTypes: data => {
      return dispatch({
        type: actionTypes.UPDATE_FILTER_TRANSATION_TYPES,
        data
      });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Transaction);
